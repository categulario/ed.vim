# Coloreado de sintaxis para el lenguaje de marcado ES

Aprende más sobre ES [aquí](https://gitlab.com/escribe-lang).

## Instalación

Si usas algo como [vim plug](https://github.com/junegunn/vim-plug) puedes añadir
una línea parecida a esta a tu `.vimrc`:

    Plug 'https://gitlab.com/escribe-lang/escribe.vim'
