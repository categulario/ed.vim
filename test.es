; File: test.es
; Author: Abraham
; Date: 2022-09-28
:meta {
    ; Comment at a higher level
    :nada {
        :si no
    }
}

:h1 Pollo

Modi porro :a(url: 'foo'){tempora} animi veniam enim tenetur voluptates. Aut vel
qui qui. Et voluptatem voluptatem quam temporibus cupiditate dignissimos sequi.
Quam omnis eaque quas; this is not a comment quia facere occaecati.

; This is a comment
; TODO finish writing this
; NOTE vim is cool
; FIXME help vim increment indentation when writing { and }

Iste repellat :b{assu:i{men}da} asperiores et. Mollitia harum facere dicta enim.
Sed laudantium cupiditate amet. Nam soluta reiciendis nostrum explicabo aut.

(url: 'foo')

Quaerat inventore distinctio vel dolor alias voluptas cum. Ut dolorem vero neque
nostrum repellat laborum veniam. Consequatur aut doloremque molestiae occaecati.
Rerum est at corrupti voluptatibus qui adipisci eligendi quo.

:allargs(str: 'str', str2: "str", bool: false, num: 34) {
    Id et rerum laborum fuga aut rerum est. Earum exercitationem vel dolores.
    Non autem quasi dolorem. Eveniet amet consequuntur voluptas id molestias
    nesciunt quidem cupiditate. Aut libero numquam dolor enim vero.
}

Cumque consectetur qui sint rerum dolorem natus quisquam temporibus. Velit aut
aut neque ipsam iste. Iusto consectetur earum sit molestiae dignissimos ut
repudiandae.
