" Vim syntax file
" Language:         escribe
" Maintainer:       Abraham Toriz
" Latest Revision:  13 September 2022

if exists("b:current_syntax")
  finish
endif

" Root elements
syn match esTag ':[a-zA-Z0-9_]\+' display nextgroup=esArgs,esBlock
syn region esBlock start="{" end="}" fold contained contains=esTag
syn region esArgs start="(" end=")" contained contains=esArgName

" Keywords
syn keyword esBools true false contained

" Comments
syn keyword esTodo contained TODO FIXME NOTE
syn match esComment "^\s*;.*$" contains=esTodo

" Named arguments
syn match esArgName '[a-zA-Z0-9_]\+:' display contained skipwhite nextgroup=esArgVal,esBools,esString

" Regular int like number with - + or nothing in front
syn match esArgVal '\d\+' contained display
syn match esArgVal '[-+]\d\+' contained display

" Floating point number with decimal no E or e (+,-)
syn match esArgVal '\d\+\.\d*' contained display
syn match esArgVal '[-+]\d\+\.\d*' contained display

" Floating point like number with E and no decimal point (+,-)
syn match esArgVal '[-+]\=\d[[:digit:]]*[eE][\-+]\=\d\+' contained display
syn match esArgVal '\d[[:digit:]]*[eE][\-+]\=\d\+' contained display

" Floating point like number with E and decimal point (+,-)
syn match esArgVal '[-+]\=\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+' contained display
syn match esArgVal '\d[[:digit:]]*\.\d*[eE][\-+]\=\d\+' contained display
syn region esString start='"' end='"' contained contains=esVarName
syn region esString start="'" end="'" contained contains=esVarName
syn match esVarName '\$[a-zA-A_]+[a-zA-Z0-9.]*' contained display

let b:current_syntax = "escribe"

hi def link esTag Statement
hi def link esArgName Label
hi def link esArgVal Number
hi def link esBools Boolean
hi def link esString String
hi def link esComment Comment
hi def link esTodo Todo
